package mismetodos;

import java.awt.image.BufferedImage;

public class UtilsValidacion {

	/**
	 * Validacion de un Email
	 * @param Email
	 * @return
	 */
	public static boolean isValidEmail(String Email) {

		// Compruebo que el email recivido no esta vacio o null
		if (Email == null || Email.trim().equals("")) {
			return false;
		}

		// Compruebo si lo que e recivido al menos tiene una arroba y un punto
		if (Email.contains(".") || Email.contains("@")) {

			// Compruebo si existe mas de una arroba
			if (UtilsString.conteoOcurrencia(Email, "@") > 1) {
				return false;
			}

			// Compruebo si hay un punto delante de la arroba o detras
			if (UtilsString.conteoOcurrencia(Email, "@.") >= 1 || UtilsString.conteoOcurrencia(Email, ".@") >= 1) {
				return false;
			}

			// Compruebo si hay algun punto detras de la arroba
			if (Email.lastIndexOf(".") < Email.lastIndexOf("@")) {
				return false;
			}

			// Por ultimo compruebo si ese punto no es justo el ultimo
			if (Email.lastIndexOf(".") == Email.length() - 1) {
				return false;
			}
			
			//Si no se da ninguna de las condiciones anteriores devuelvo un true
			return true;
			
		} else
			return false;
	}

	/**
	 * Este metodo sirve para validar codigos
	 * 
	 * En este ejemplo las tres primeras letras empiezan por mayuscula
	 * [A-Z][3,]. 
	 * 
	 * https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/regex/Pattern.html#sum
	 * 
	 * @param str
	 * @param exRegular
	 * @return
	 */
	public boolean isValidCode(String str, String exRegular) {
		
		return str.matches(exRegular);
		
	}
	
	/**
	 * Este metodo comprueba si el tamaño de una 
	 * imagen se adacta a lo que queremos
	 * @param img
	 * @param width
	 * @param height
	 * @return
	 */
	public boolean isImageSizeCorrect(byte[] img, int width, int height) {
		
		BufferedImage imgbuff = UtilsImg.arrayByteToBufferedImage(img);
		
		if (imgbuff.getWidth() == width && imgbuff.getHeight() == height) {
			return true; //La imagen cumple con los parametros
		}
		
		return false;
	}
	
	/**
	 * no terminado
	 * @param fecha
	 * @return
	 */
	public boolean isValidFecha (String fecha) {
		return true;
	}
}
