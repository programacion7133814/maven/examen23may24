package mismetodos;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.File;
import java.nio.file.Files;

import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileFilter;

public class UtilsMenu {

	/**
	 * Una platilla para menus que se puede modificar facilmente
	 * 
	 * @return
	 */
	public static int pantillaMenu() {

		int num = UtilsMenu.pidenumeromenuV2(
				"Opciones \n" + "1. opc \n" + "2. opc \n" + "3. opc \n" + "4. opc \n" + "5. opc \n" + "6. opc \n");

		return num;
	}

	/**
	 * Muentra un menu que pide texto por pantalla
	 * 
	 * @param s
	 * @return
	 */
	public static String pideTextoMenu(String s) {

		String str = JOptionPane.showInputDialog(s);

		return str;
	}

	/**
	 * Muestra un menu al que se le inserta un numero
	 * 
	 * @param s
	 * @return
	 */
	public static int pidenumeromenu(String s) {
		int n = 0;
		boolean esNumeroCorrecto;

		do {
			esNumeroCorrecto = false;
			try {
				n = Integer.parseInt(JOptionPane.showInputDialog(s));
				esNumeroCorrecto = true;
			} catch (Exception e) {
				System.out.println("No ha introducido un número");
			}
		} while (esNumeroCorrecto == false);

		return n;
	}

	public static int pidenumeromenuV2(String s) {
		int n = 0;
		boolean esNumeroCorrecto;
		boolean cancelado = false;

		do {
			esNumeroCorrecto = false;
			try {
				String input = JOptionPane.showInputDialog(s);

				// Verificar si se ha cancelado la entrada (input es null)
				if (input == null) {
					cancelado = true;
					break; // Salir del bucle
				}

				n = Integer.parseInt(input);
				esNumeroCorrecto = true;
			} catch (NumberFormatException e) {
				UtilsMenu.mensajemenu("No se a introducido un numero valido");
			}
		} while (!esNumeroCorrecto);

		// Si se ha cancelado la entrada, puedes manejarlo según tus necesidades
		if (cancelado) {
			// Puedes elegir qué hacer en este caso, por ejemplo, salir del programa
			System.exit(0);
		}

		return n;
	}

	public static Float pidenumeromenuV2float(String s) {
		float n = 0;
		boolean esNumeroCorrecto;
		boolean cancelado = false;

		do {
			esNumeroCorrecto = false;
			try {
				String input = JOptionPane.showInputDialog(s);

				// Verificar si se ha cancelado la entrada (input es null)
				if (input == null) {
					cancelado = true;
					break; // Salir del bucle
				}

				n = Float.parseFloat(input);
				esNumeroCorrecto = true;
			} catch (NumberFormatException e) {
				UtilsMenu.mensajemenu("No se a introducido un numero valido");
			}
		} while (!esNumeroCorrecto);

		// Si se ha cancelado la entrada, puedes manejarlo según tus necesidades
		if (cancelado) {
			// Puedes elegir qué hacer en este caso, por ejemplo, salir del programa
			System.exit(0);
		}

		return n;
	}

	/**
	 * Muestra un menu al que se le inserta un numero
	 * 
	 * @param s
	 * @return
	 */
	public static double pidenumeromenudouble(String s) {
		double n = 0;
		boolean esNumeroCorrecto;

		do {
			esNumeroCorrecto = false;
			try {

				n = Double.parseDouble(JOptionPane.showInputDialog(s));
				esNumeroCorrecto = true;
			} catch (Exception e) {
				System.out.println("No ha introducido un número");
			}
		} while (esNumeroCorrecto == false);

		return n;
	}

	/**
	 * Muestra una ventana de cierre y se cierra si se pulsa aceptar
	 * 
	 * @param mensaje
	 * @param titulo
	 * @param v
	 * @return 
	 */
	public static boolean cierraVentana(String mensaje, String titulo, JFrame v) {
		String[] opciones = { "Aceptar", "Cancelar" };

		// sintaxis de showOptionDialog ¿Sobre donde?, ¿Que texto?, ¿Que titulo?,
		// ¿Que tipo?, ¿Que tipo de mesaje?, cosa, opciones, por defecto

		int eleccion = JOptionPane.showOptionDialog(v, mensaje, titulo, JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, opciones, "Aceptar");

		// compare si lo que a salido de eleccion es igual al YES_OPTION
		if (eleccion == JOptionPane.YES_OPTION) {
			System.exit(0);
			return true;
		}
		return false;
	}

	/**
	 * metodo para realiazar una pregunta al usuario y devolver falso o verdadero
	 * 
	 * @param mensaje
	 * @param titulo
	 * @param v
	 * @return
	 */
	public static boolean pregunta(String mensaje, String titulo, JFrame v) {
		String[] opciones = { "Aceptar", "Cancelar" };

		// sintaxis de showOptionDialog ¿Sobre donde?, ¿Que texto?, ¿Que titulo?,
		// ¿Que tipo?, ¿Que tipo de mesaje?, cosa, opciones, por defecto

		int eleccion = JOptionPane.showOptionDialog(v, mensaje, titulo, JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, opciones, "Aceptar");

		// compare si lo que a salido de eleccion es igual al YES_OPTION
		if (eleccion == JOptionPane.YES_OPTION) {
			return true;
		}

		else {
			return false;
		}
	}

	/**
	 * Muestra un mensaje en un menu
	 * @param s
	 */
	public static void mensajemenu(String s) {
		JOptionPane.showMessageDialog(null, s);
	}
	
	/**
	 * Este metodo muestra un fileChooser para que puedas elegir una imagen
	 * Este metodo contiene una clase anima con una serie de metodos en su
	 * interior para modificar
	 * 
	 * Cuando se elija una imagen si es valida esta se llamara a un metodo para
	 * cargar la imagen
	 */
	public static void selectImagen(byte[] imagen, JScrollPane jspImage) {
		JFileChooser jchoose = new JFileChooser();
		
		jchoose.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		jchoose.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				// TODO Auto-generated method stub
				return "Archivos de imagen *.jpg *.png *.gif";
			}
			
			@Override
			public boolean accept(File f) {
				if (f.isDirectory() || (f.isFile() && (f.getAbsolutePath().toLowerCase().endsWith(".jpg") || 
						f.getAbsolutePath().toLowerCase().endsWith(".jpeg")|| 
						f.getAbsolutePath().toLowerCase().endsWith(".png")|| 
						f.getAbsolutePath().toLowerCase().endsWith(".gif")))) 
					return true;
				
				return false;
			}
		});
		
		// Abro el diálogo para la elección del usuario
				int seleccionUsuario = jchoose.showOpenDialog(null);
				
				if (seleccionUsuario == JFileChooser.APPROVE_OPTION) {
					File fichero = jchoose.getSelectedFile();
					
					if (fichero.isFile()) {
						try {
							
							imagen = Files.readAllBytes(fichero.toPath());
							cargarImagen(imagen, jspImage);
							
						}
						catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}
	}
	
	/**
	 * Este metodo es capaz de crargar una imagen en un jota sScollPane
	 * @param o
	 */
	public static void cargarImagen(byte[] imagen, JScrollPane jspImage) {
		// Creo un objeto imagenIcom que sera mi imagen en bytes
		if (imagen != null) {
			JLabel jimg = new JLabel(new ImageIcon(imagen));
			
			// Ahora añado en el scrollpane el label
			jspImage.setViewportView(jimg);
		}
		
	}
	
	/**
	 * Este metodo muentra un panel para seleccionar un color
	 */
	public static void selectColor(JColorChooser jColor, JPanel panel) {
		
		Color co = jColor.showDialog(panel, "Selecion de color", Color.white);
		
		cargarColor(co, panel);
	}

	/**
	 * Este metodo recibe un color u lo muestra
	 * @param co
	 */
	public static void cargarColor(Color co, JPanel panel) {
		if (co != null) {
			String strColor = "#"+Integer.toHexString(co.getRGB()).substring(2);
			panel.setBackground(co);
		}
		
	}
	
	/**
	 * Cargar un color a patir de un string
	 * @param str
	 */
	public static void cargarColor(String str, JPanel panel) {
		
		if (str != null && !(str.trim().equals(""))) {
			panel.setBackground(Color.decode(str));
		}
		else panel.setBackground(Color.white);
	}
	
	/**
	 * Integra un jpanel en un jdialog 
	 * @param p
	 * @param tituloVentana
	 */
	public static void jdialodpanel(JPanel p, String tituloVentana) {
		JDialog jd = new JDialog();

		jd.setTitle(tituloVentana);
		jd.setContentPane(p);
		jd.pack();
		// No se puede hacer click en la ventana principal hasta que se cierre esta
		jd.setModal(true);
		// El dialogo aparecera en el centro de la pantalla
		jd.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width) / 2 - jd.getWidth() / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height) / 2 - jd.getHeight() / 2);
		jd.setVisible(true);
	}
	
	/**
	 * Permite seleccionar los ficheros y devuelve su ruta
	 * Nota:
	 * Puedes filtrar ficheros con jcChooser.setFilfeFilter y dentro una nueva clase anonima que se llama fileFilter
	 * 
	 * Utiliza el metodo getDescription y accept con un override
	 * @return 
	 */
	public static String selectfiles(boolean DirectoriesOnly) {
		JFileChooser jcChooser = new JFileChooser();
		
		if (DirectoriesOnly) {
			//Solo accepcto directorios es decir carpetas
			jcChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}

		// Abro el diálogo para la elección del usuario
		int seleccionUsuario = jcChooser.showOpenDialog(null);
		
		if (seleccionUsuario == JFileChooser.APPROVE_OPTION) {
			File fichero = jcChooser.getSelectedFile();
			String ruta = fichero.getAbsolutePath(); // Devuelve la ruta del fichero
			
			return ruta;
		}
		return null;

	}
	

}
