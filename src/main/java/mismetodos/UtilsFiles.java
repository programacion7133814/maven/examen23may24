package mismetodos;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JProgressBar;

public class UtilsFiles {

	/**
	 * Este metodos copiar archivos de una carpeta a otra 
	 * Con el boleano a true corta
	 * Se le puede pasar una progressbar para que muestre el progreso
	 */
	public static void pasarFicheros(String rutaorigen, String rutadestino, boolean cortar, JProgressBar barraDeProgreso) {
		File origen = new File(rutaorigen); // Carpeta origen
		File destino = new File(rutadestino); // Carpeta Destino
		
		if (origen.exists() && origen.isDirectory()) {
			
			File[] archivos = origen.listFiles();
			
			if (archivos != null) {
				int cantArchivos = archivos.length;
				int copiados = 0;
				
				
				try {
					
					for (File file : archivos) {
						Path origenPath = file.toPath(); //Ruta fichero de origen
						Path destinoPath = new File(destino, file.getName()).toPath(); //Ruta fichero destino
						
						Files.copy(origenPath, destinoPath, StandardCopyOption.REPLACE_EXISTING);
						copiados++;
						
						if (barraDeProgreso != null) {
							
							int progreso = (int) ((double) copiados / cantArchivos * 100);
							barraDeProgreso.setValue(progreso);
						}
						
						if (cortar) file.delete();
					}
					
				} catch (IOException e) {
					UtilsMenu.mensajemenu("Error al copiar");
				}
			}
			
			else {
				UtilsMenu.mensajemenu("No hay archivos en la carpeta de origen");
			}
			
		}
		
		else {
			UtilsMenu.mensajemenu("La carpeta de origen no existe o no es un directorio");
		}
		
	}
	
	/**
	 * Busca uno ficheros segun una busqueda
	 * @param directorio
	 * @param busqueda
	 * @return
	 */
	public static List<File> buscarFicheros(String directorio, String busqueda) {
		List<File>  archivosEncontrados = new ArrayList<File>();
		File carpeta = null;
		
		if (!directorio.trim().equals("")) {
			carpeta = new File(directorio);
			if (!carpeta.isDirectory()) {
				UtilsMenu.mensajemenu("La ruta especificada no es un directorio");
				return archivosEncontrados;
			}
			
			//Creo un array de archivos con en directorio especificado
			File[] archivos = carpeta.listFiles();
			if (archivos != null) {
				for (File file : archivos) {
					
					//Compruebo si es un archio y si contiene algo escrito en la busqueda
					if (file.getName().toLowerCase().contains(busqueda.toLowerCase())) {
						archivosEncontrados.add(file);
					}
					
				}
			}
		}
		
		
		return archivosEncontrados;
	}
	
	/**
	 * Busca uno ficheros segun una busqueda
	 * @param directorio
	 * @param busqueda
	 * @return
	 */
	public static List<File> buscarFicheros(String directorio, String busqueda, double kilobytes) {
		List<File>  archivosEncontrados = new ArrayList<File>();
		File carpeta = null;
		
		if (!directorio.trim().equals("")) {
			carpeta = new File(directorio);
			if (!carpeta.isDirectory()) {
				UtilsMenu.mensajemenu("La ruta especificada no es un directorio");
				return archivosEncontrados;
			}
			
			//Creo un array de archivos con en directorio especificado
			File[] archivos = carpeta.listFiles();
			if (archivos != null) {
				for (File file : archivos) {
					
					//Compruebo si es un archio y si contiene algo escrito en la busqueda
					if (file.getName().toLowerCase().contains(busqueda.toLowerCase()) && ficheromayortamanio(file, kilobytes)) {
						archivosEncontrados.add(file);
					}
				}
			}
		}
		
		
		return archivosEncontrados;
	}
	
	/**
	 * Devuelve el tamaño del fichero en KB
	 * @param fichero
	 * @return
	 */
	public static double tamaniofichero(File fichero) {
		if (fichero != null) {
		long tamBytes = fichero.length();
		double tamkb = tamBytes / 1024.0;
		
		return tamkb;
		}
		return 0;
	}
	
	/**
	 * Compuevo si el tamaño del fichero es igual al especificado
	 * @param fichero
	 * @param tam
	 * @return
	 */
	public static boolean validSizeFile(File fichero, double tam) {
		if (fichero != null) {
			
			double tamkb = tamaniofichero(fichero);
			if (tamkb == tam) return true;
			
			else return false;
		}
		return false;
	}
	
	/**
	 * Compuevo si el tamaño del fichero es menor al especificado
	 * @param fichero
	 * @param tam
	 * @return
	 */
	public static boolean ficheromenortamanio(File fichero, double tam) {
		if (fichero != null) {
			
			double tamkb = tamaniofichero(fichero);
			if (tamkb < tam) return true;
			
			else return false;
		}
		return false;
	}
	
	/**
	 * Compuevo si el tamaño del fichero es mayor al especificado
	 * @param fichero
	 * @param tam
	 * @return
	 */
	public static boolean ficheromayortamanio(File fichero, double tam) {
		if (fichero != null) {
			
			double tamkb = tamaniofichero(fichero);
			if (tamkb > tam) return true;
			
			else return false;
		}
		return false;
	}
}
