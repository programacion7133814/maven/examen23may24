package mismetodos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

public class UtilsFormatedjtf {

	/**
	 * Formatea la fecha en un jformatedtextfiel llamando a otro metodo que los hace
	 * @param jftf
	 * @param formato
	 * @return
	 */
	public static JFormattedTextField formatedTextfield(String formato) {
		if (formato == null) {
			formato = "dd/MM/yyyy";
			return formTextfield(formato);
		}
		return formTextfield(formato);
	}
	
	
	/**
	 * **********************************************************************
	 * Nota: Esta hecho asi por que no deja usar la variable cuando uso el if 
	 * Formatea la fecha en un jformatedtextfiel
	 * @param jftf
	 * @param formato
	 * @return
	 */
	private static JFormattedTextField formTextfield(String formato) {
		
		JFormattedTextField jftf = new JFormattedTextField(
				new JFormattedTextField.AbstractFormatter() {
			SimpleDateFormat sdf = new SimpleDateFormat(formato);

			@Override
			public String valueToString(Object value) throws ParseException {
				if (value != null && value instanceof Date) {
					return sdf.format(((Date) value));
				}
				return null;
			}

			@Override
			public Object stringToValue(String text) throws ParseException {
				try {
					return sdf.parse(text);
				} catch (Exception e) {
				//	e.printStackTrace();
					JOptionPane.showMessageDialog(null, "La fecha no tiene el formato correcto " + formato);
					return null;
				}
			}
		});
		
		jftf.setValue(new Date());
		return jftf;
	}
}
