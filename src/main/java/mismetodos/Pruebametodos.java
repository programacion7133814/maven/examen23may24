package mismetodos;

import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Pruebametodos {
	
	public static void main(String[] args) {
        JFrame frame = new JFrame("JTable Update Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Datos iniciales
        Object[][] data = {
            {"John", 30},
            {"Jane", 25},
            {"Doe", 40}
        };
        String[] columnNames = {"Name", "Age"};

        // Crear el modelo de la tabla
        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        JTable table = new JTable(model);

        // Botón para actualizar los datos
        JButton updateButton = new JButton("Actualizar Datos");
        updateButton.addActionListener(e -> {
            // Modificar los datos
            model.setValueAt("Alice", 0, 0); // Cambia el nombre "John" a "Alice"
            model.setValueAt(35, 0, 1); // Cambia la edad de "Alice" a 35
        });

        // Añadir la tabla y el botón al marco
        frame.getContentPane().add(new JScrollPane(table));
        frame.getContentPane().add(updateButton, "South");
        frame.pack();
        frame.setVisible(true);
    }
}

