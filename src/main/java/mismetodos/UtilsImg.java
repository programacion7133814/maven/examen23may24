package mismetodos;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class UtilsImg {

	/**
	 * Este metodo es capaz de combertir un array de bytes
	 * en una imagenbuffereada
	 * @param img
	 * @return
	 */
	public static BufferedImage arrayByteToBufferedImage(byte[] img) {
		
		if (img != null) {
			try {
				
				ByteArrayInputStream bis = new ByteArrayInputStream(img);
				BufferedImage imgbuff = ImageIO.read(bis);
				return imgbuff;
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
}
