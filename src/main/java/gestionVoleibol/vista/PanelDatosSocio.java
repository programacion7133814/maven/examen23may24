package gestionVoleibol.vista;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JToolBar;

import gestionVoleibol.drivers.DriverEquipo;
import gestionVoleibol.drivers.DriverSocio;
import gestionVoleibol.entidades.Equipo;
import gestionVoleibol.entidades.Socio;
import mismetodos.UtilsFecha;
import mismetodos.UtilsFormatedjtf;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.GridBagLayout;
import javax.swing.JComboBox;
import java.awt.GridBagConstraints;
import javax.swing.JCheckBox;
import java.awt.Insets;
import java.util.Date;
import java.util.List;

import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelDatosSocio extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField jtfSegundoApellido;
	private JTextField jtfPrimerApellido;
	private JTextField jtfNombre;
	private JComboBox<Equipo> jcEquipo;
	private JButton btnPrimero;
	private JButton btnAnterior;
	private JButton btnSiguiente;
	private JButton btnUtimo;
	private JButton btnGuardar;
	private JButton btnNuevo;
	private JButton btnEliminar;
	private JFormattedTextField jtfFecha;
	private JSlider sliderAntiguedad;
	private JLabel labedAntigedad;
	private JCheckBox chckbxActivo;
	private int id;
	
	/**
	 * Create the panel.
	 */
	public PanelDatosSocio() {
		setLayout(new BorderLayout(0, 0));
		
		JToolBar toolBar = new JToolBar();
		add(toolBar, BorderLayout.NORTH);
		
		btnPrimero = new JButton("");
		btnPrimero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cargarPrimero();
			}
		});
		btnPrimero.setIcon(new ImageIcon(PanelDatosSocio.class.getResource("/mismetodos/res/gotostart.png")));
		toolBar.add(btnPrimero);
		
		btnAnterior = new JButton("");
		btnAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cargarAnterior();
			}
		});
		btnAnterior.setIcon(new ImageIcon(PanelDatosSocio.class.getResource("/mismetodos/res/previous.png")));
		toolBar.add(btnAnterior);
		
		btnSiguiente = new JButton("");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cargarSiguiente();
			}
		});
		btnSiguiente.setIcon(new ImageIcon(PanelDatosSocio.class.getResource("/mismetodos/res/next.png")));
		toolBar.add(btnSiguiente);
		
		btnUtimo = new JButton("");
		btnUtimo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cargaUtimo();
			}
		});
		btnUtimo.setIcon(new ImageIcon(PanelDatosSocio.class.getResource("/mismetodos/res/gotoend.png")));
		toolBar.add(btnUtimo);
		
		btnGuardar = new JButton("");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				guardar();
			}
		});
		btnGuardar.setIcon(new ImageIcon(PanelDatosSocio.class.getResource("/mismetodos/res/guardar.png")));
		toolBar.add(btnGuardar);
		
		btnNuevo = new JButton("");
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nuevo();
			}
		});
		btnNuevo.setIcon(new ImageIcon(PanelDatosSocio.class.getResource("/mismetodos/res/nuevo.png")));
		toolBar.add(btnNuevo);
		
		btnEliminar = new JButton("");
		btnEliminar.setIcon(new ImageIcon(PanelDatosSocio.class.getResource("/mismetodos/res/eliminar.png")));
		toolBar.add(btnEliminar);
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		jtfNombre = new JTextField();
		GridBagConstraints gbc_jtfNombre = new GridBagConstraints();
		gbc_jtfNombre.insets = new Insets(0, 0, 5, 5);
		gbc_jtfNombre.fill = GridBagConstraints.HORIZONTAL;
		gbc_jtfNombre.gridx = 1;
		gbc_jtfNombre.gridy = 0;
		panel.add(jtfNombre, gbc_jtfNombre);
		jtfNombre.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Primer apellido");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		jtfPrimerApellido = new JTextField();
		GridBagConstraints gbc_jtfPrimerApellido = new GridBagConstraints();
		gbc_jtfPrimerApellido.insets = new Insets(0, 0, 5, 5);
		gbc_jtfPrimerApellido.fill = GridBagConstraints.HORIZONTAL;
		gbc_jtfPrimerApellido.gridx = 1;
		gbc_jtfPrimerApellido.gridy = 1;
		panel.add(jtfPrimerApellido, gbc_jtfPrimerApellido);
		jtfPrimerApellido.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Segundo Apellido");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		panel.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		jtfSegundoApellido = new JTextField();
		GridBagConstraints gbc_jtfSegundoApellido = new GridBagConstraints();
		gbc_jtfSegundoApellido.insets = new Insets(0, 0, 5, 5);
		gbc_jtfSegundoApellido.fill = GridBagConstraints.HORIZONTAL;
		gbc_jtfSegundoApellido.gridx = 1;
		gbc_jtfSegundoApellido.gridy = 2;
		panel.add(jtfSegundoApellido, gbc_jtfSegundoApellido);
		jtfSegundoApellido.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Fecha de nacimiento");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 3;
		panel.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		jtfFecha = UtilsFormatedjtf.formatedTextfield("dd/MM/yyyy");
		GridBagConstraints gbc_jtfFecha = new GridBagConstraints();
		gbc_jtfFecha.insets = new Insets(0, 0, 5, 5);
		gbc_jtfFecha.fill = GridBagConstraints.HORIZONTAL;
		gbc_jtfFecha.gridx = 1;
		gbc_jtfFecha.gridy = 3;
		panel.add(jtfFecha, gbc_jtfFecha);
		
		JLabel lblNewLabel_4 = new JLabel("Antiguedad");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 4;
		panel.add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		sliderAntiguedad = new JSlider(JSlider.HORIZONTAL, 1, 200, 18);
		GridBagConstraints gbc_sliderAntiguedad = new GridBagConstraints();
		gbc_sliderAntiguedad.fill = GridBagConstraints.HORIZONTAL;
		gbc_sliderAntiguedad.insets = new Insets(0, 0, 5, 5);
		gbc_sliderAntiguedad.gridx = 1;
		gbc_sliderAntiguedad.gridy = 4;
		panel.add(sliderAntiguedad, gbc_sliderAntiguedad);
		
		labedAntigedad = new JLabel("x años");
		GridBagConstraints gbc_labedAntigedad = new GridBagConstraints();
		gbc_labedAntigedad.insets = new Insets(0, 0, 5, 0);
		gbc_labedAntigedad.gridx = 2;
		gbc_labedAntigedad.gridy = 4;
		panel.add(labedAntigedad, gbc_labedAntigedad);
		
		chckbxActivo = new JCheckBox("Socio en activo");
		GridBagConstraints gbc_chckbxActivo = new GridBagConstraints();
		gbc_chckbxActivo.anchor = GridBagConstraints.WEST;
		gbc_chckbxActivo.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxActivo.gridx = 1;
		gbc_chckbxActivo.gridy = 5;
		panel.add(chckbxActivo, gbc_chckbxActivo);
		
		JLabel lblNewLabel_5 = new JLabel("Equipo");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel_5.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 6;
		panel.add(lblNewLabel_5, gbc_lblNewLabel_5);
		
		jcEquipo = new JComboBox<Equipo>();
		GridBagConstraints gbc_jcEquipo = new GridBagConstraints();
		gbc_jcEquipo.insets = new Insets(0, 0, 0, 5);
		gbc_jcEquipo.fill = GridBagConstraints.HORIZONTAL;
		gbc_jcEquipo.gridx = 1;
		gbc_jcEquipo.gridy = 6;
		panel.add(jcEquipo, gbc_jcEquipo);

		cargarPrimero();
	}
	
	/**
	 * 
	 * @param s
	 */
	private void motrar(Socio s) {
		cargarEquipos();
		
		if (s != null) {
			id = s.getId();
			jtfNombre.setText(s.getNombre());
			jtfPrimerApellido.setText(s.getApellido1());
			jtfSegundoApellido.setText(s.getApellido2());
			
			if (s.getFechaNacimiento() != null) {
				jtfFecha.setText(UtilsFecha.DateAString(s.getFechaNacimiento(), "dd/MM/yyyy"));
				
			}
			
			sliderAntiguedad.setValue(s.getAntiguedadAnios());
			labedAntigedad.setText("Anios" + sliderAntiguedad.getValue());
			
			
			chckbxActivo.setSelected(s.getActivo());
			
			for (int i = 0; i < jcEquipo.getItemCount(); i++) {
				
				Equipo e = (Equipo) jcEquipo.getItemAt(i);
				
				if (e == s.getEquipo()) {
					jcEquipo.setSelectedIndex(i);
				}
			}
		}
		
		

	}
	
	private void cargarEquipos() {
		List<Equipo> le = (List<Equipo>) DriverEquipo.getInstance().getAll();
		
		jcEquipo.removeAllItems();
		
		for (Equipo equipo : le) {
			jcEquipo.addItem(equipo);
		}
	}
	
	private void cargarPrimero() {
		Socio s = (Socio) DriverSocio.getInstance().findFirst();
		
		motrar(s);
	}
	
	private void cargaUtimo() {
		Socio s = (Socio) DriverSocio.getInstance().findlast();
		
		motrar(s);
		
	}
	
	private void cargarSiguiente() {
		Socio s = (Socio) DriverSocio.getInstance().findNext(id);
		
		motrar(s);
	}
	
	private void cargarAnterior() {
		Socio s = (Socio) DriverSocio.getInstance().findPrevius(id);
		
		motrar(s);
	}
	
	private void guardar() {
		Socio s = converSocio();
	}
	
	private Socio converSocio() {
		Socio s = new Socio();
		
		s.setId(id);
		s.setNombre(jtfNombre.getText());
		s.setApellido1(jtfPrimerApellido.getText());
		s.setApellido2(jtfSegundoApellido.getText());
		s.setFechaNacimiento(UtilsFecha.formateoFecha(jtfFecha.getText(), "dd/MM/yyyy"));
		s.setAntiguedadAnios(sliderAntiguedad.getValue());
		s.setActivo(chckbxActivo.isSelected());
		s.setEquipo((Equipo) jcEquipo.getSelectedItem());
		
		return s;
	}
	
	private void nuevo() {
		
		id = 0;
		jtfNombre.setText("");
		jtfPrimerApellido.setText("");
		jtfSegundoApellido.setText("");
		Date d = new Date();
		jtfFecha.setText(UtilsFecha.DateAString(d, "dd/MM/yyyy"));
		sliderAntiguedad.setValue(0);
		chckbxActivo.setSelected(false);
		jcEquipo.setSelectedIndex(0);
	}

}
