package gestionVoleibol.vista;

import javax.swing.JPanel;
import java.awt.GridBagLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.List;

import javax.swing.JScrollPane;

import gestionVoleibol.drivers.DriverEquipo;
import gestionVoleibol.entidades.Equipo;

import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PanelClasificacion extends JPanel {

	private static final long serialVersionUID = 1L;
	private DefaultListModel<Equipo> lmEquipos = new DefaultListModel<Equipo>();
	private JList<Equipo> jlistEquipos;
	
	/**
	 * Create the panel.
	 */
	public PanelClasificacion() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblNewLabel = new JLabel("Claseificacion");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridwidth = 3;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		add(lblNewLabel, gbc_lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 6;
		gbc_scrollPane.gridwidth = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		add(scrollPane, gbc_scrollPane);
		
		jlistEquipos = new JList<Equipo>(lmEquipos);
		scrollPane.setViewportView(jlistEquipos);
		
		JButton btnReset = new JButton("Resetear");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reset();
			}
		});
		GridBagConstraints gbc_btnReset = new GridBagConstraints();
		gbc_btnReset.insets = new Insets(0, 0, 5, 0);
		gbc_btnReset.gridx = 2;
		gbc_btnReset.gridy = 2;
		add(btnReset, gbc_btnReset);
		
		JButton btnArriba = new JButton("Arriba");
		btnArriba.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				arriba();
			}
		});
		GridBagConstraints gbc_btnArriba = new GridBagConstraints();
		gbc_btnArriba.insets = new Insets(0, 0, 5, 0);
		gbc_btnArriba.gridx = 2;
		gbc_btnArriba.gridy = 3;
		add(btnArriba, gbc_btnArriba);
		
		JButton btnAbajo = new JButton("Abajo");
		btnAbajo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				abajo();
			}
		});
		GridBagConstraints gbc_btnAbajo = new GridBagConstraints();
		gbc_btnAbajo.insets = new Insets(0, 0, 5, 0);
		gbc_btnAbajo.gridx = 2;
		gbc_btnAbajo.gridy = 4;
		add(btnAbajo, gbc_btnAbajo);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				eliminarSeleccionado();
			}
		});
		GridBagConstraints gbc_btnEliminar = new GridBagConstraints();
		gbc_btnEliminar.insets = new Insets(0, 0, 5, 0);
		gbc_btnEliminar.gridx = 2;
		gbc_btnEliminar.gridy = 5;
		add(btnEliminar, gbc_btnEliminar);
		
		cargarEquipos();
	}
	
	private void cargarEquipos() {
		lmEquipos.removeAllElements();
		
		List<Equipo> lE = (List<Equipo>) DriverEquipo.getInstance().getAll();
		
		for (Equipo equipo : lE) {
			lmEquipos.addElement(equipo);
		}
	}
	
	private void eliminarSeleccionado() {
		
		for (int i = jlistEquipos.getSelectedIndices().length - 1; i >= 0; i--) {
			
			lmEquipos.removeElementAt(jlistEquipos.getSelectedIndices()[i]);
		}
	}
	
	private void reset() {
		cargarEquipos();
	}
	
	private void arriba() {
		
		int indices = lmEquipos.getSize() - 1;
		int indiceActual = jlistEquipos.getSelectedIndex();
		
		if (indiceActual <= indices && indiceActual > 0) {
			Equipo e1 = lmEquipos.get(indiceActual);
			Equipo e2 = lmEquipos.get(indiceActual - 1);
			
			lmEquipos.set(indiceActual, e2);
			lmEquipos.set(indiceActual - 1, e1);
			
			jlistEquipos.setSelectedIndex(indiceActual - 1);
		}
		
	}
	
	private void abajo() {
		
		int indices = lmEquipos.getSize() - 1;
		int indiceActual = jlistEquipos.getSelectedIndex();
		
		if (indiceActual <= indices && indiceActual < lmEquipos.getSize() - 1) {
			Equipo e1 = lmEquipos.get(indiceActual);
			Equipo e2 = lmEquipos.get(indiceActual + 1);
			
			lmEquipos.set(indiceActual, e2);
			lmEquipos.set(indiceActual + 1, e1);
			
			jlistEquipos.setSelectedIndex(indiceActual + 1);
		}
		
	}
	

}
