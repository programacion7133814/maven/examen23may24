package gestionVoleibol.vista;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JScrollPane;

import gestionVoleibol.drivers.DriverEquipo;
import gestionVoleibol.drivers.DriverSocio;
import gestionVoleibol.entidades.Equipo;
import gestionVoleibol.entidades.Socio;
import mismetodos.UtilsFecha;
import mismetodos.UtilsMenu;
import tabla.MiDefaultTableCellRenderer;
import tabla.MiTablaModel;
import tabla.MiTableHeaderCellRenderer;

import java.awt.GridBagConstraints;
import javax.swing.JRadioButton;
import java.awt.Insets;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import com.mysql.cj.x.protobuf.MysqlxDatatypes.Array;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;

public class PanelSociosDelEquipo extends JPanel {

	private static final long serialVersionUID = 1L;
	private JComboBox<Equipo> jcEquipo;
	private JTable table;
	private TableModel tm;
	private JScrollPane scrollPane;
	private ButtonGroup bg;
	private JRadioButton rdbtnOrdenarporFecha;
	private JRadioButton rdbtnOrdenarPorNombre;
	private JRadioButton rdbtnOrdenarPorPrimerApellido;
	private JRadioButton rbtnOrdenaPorSegundoApellido;
	
	/**
	 * Create the panel.
	 */
	public PanelSociosDelEquipo() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblNewLabel_1 = new JLabel("Socios del equipo");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.gridwidth = 4;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		add(lblNewLabel_1, gbc_lblNewLabel_1);

		JLabel lblNewLabel = new JLabel("Equipo");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		add(lblNewLabel, gbc_lblNewLabel);

		jcEquipo = new JComboBox<Equipo>();
		jcEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				table = pintarTabla(tm, table);
				scrollPane.setViewportView(table);
			}
		});
		GridBagConstraints gbc_jcEquipo = new GridBagConstraints();
		gbc_jcEquipo.gridwidth = 3;
		gbc_jcEquipo.insets = new Insets(0, 0, 5, 0);
		gbc_jcEquipo.fill = GridBagConstraints.HORIZONTAL;
		gbc_jcEquipo.gridx = 1;
		gbc_jcEquipo.gridy = 1;
		add(jcEquipo, gbc_jcEquipo);

		rdbtnOrdenarPorNombre = new JRadioButton("Ordenar por nombre");
		GridBagConstraints gbc_rdbtnOrdenarPorNombre = new GridBagConstraints();
		gbc_rdbtnOrdenarPorNombre.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnOrdenarPorNombre.gridx = 1;
		gbc_rdbtnOrdenarPorNombre.gridy = 2;
		add(rdbtnOrdenarPorNombre, gbc_rdbtnOrdenarPorNombre);
		rdbtnOrdenarPorNombre.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				table = pintarTabla(tm, table);
				scrollPane.setViewportView(table);
			}
		});

		rdbtnOrdenarPorPrimerApellido = new JRadioButton("OrdenarPorPrimerApellido");
		GridBagConstraints gbc_rdbtnOrdenarPorPrimerApellido = new GridBagConstraints();
		gbc_rdbtnOrdenarPorPrimerApellido.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnOrdenarPorPrimerApellido.gridx = 2;
		gbc_rdbtnOrdenarPorPrimerApellido.gridy = 2;
		add(rdbtnOrdenarPorPrimerApellido, gbc_rdbtnOrdenarPorPrimerApellido);

		rbtnOrdenaPorSegundoApellido = new JRadioButton("Ordenar por segundo apellido");
		GridBagConstraints gbc_rdbtnNewRadioButton_1 = new GridBagConstraints();
		gbc_rdbtnNewRadioButton_1.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnNewRadioButton_1.gridx = 1;
		gbc_rdbtnNewRadioButton_1.gridy = 3;
		add(rbtnOrdenaPorSegundoApellido, gbc_rdbtnNewRadioButton_1);

		rdbtnOrdenarporFecha = new JRadioButton("Ordenar por fecha de nacimiento");
		GridBagConstraints gbc_rdbtnOrdenarporFecha = new GridBagConstraints();
		gbc_rdbtnOrdenarporFecha.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnOrdenarporFecha.gridx = 2;
		gbc_rdbtnOrdenarporFecha.gridy = 3;
		add(rdbtnOrdenarporFecha, gbc_rdbtnOrdenarporFecha);

		scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 4;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 4;
		add(scrollPane, gbc_scrollPane);

		cargarEquipos();
		
		table = pintarTabla(tm, table);
		scrollPane.setViewportView(table);

		bg = new ButtonGroup();
		bg.add(rdbtnOrdenarporFecha);
		bg.add(rbtnOrdenaPorSegundoApellido);
		bg.add(rdbtnOrdenarPorPrimerApellido);
		bg.add(rdbtnOrdenarPorNombre);
		
	}

	private void cargarEquipos() {
		List<Equipo> le = (List<Equipo>) DriverEquipo.getInstance().getAll();

		jcEquipo.removeAllItems();

		for (Equipo equipo : le) {
			jcEquipo.addItem(equipo);
		}
	}

////////////////////////////////////TABLA//////////////////////////////////////////////
	/**
	 * Devuelde los titulos de la columnas Colocala la misma cantidad de columnas
	 * como datos quieras imprimir
	 * 
	 * Ejemplo : return new String[] { "id", "nombre", "apellido1", "apellido2",
	 * "dni", "direccion", "email", "telefono" };
	 * 
	 * @return
	 */
	public String[] getTitulosColumnas() {
		return new String[] { "Nombre", "Apellido1", "Apellido2", "Fecha de nacimiento"};
	}

	/**
	 * Este petodo devolvera los metodos de la tabla
	 */
	public Object[][] getDatosDeTabla() {
		Object[][] datos = null;
		String columna = null;
		
		if (rbtnOrdenaPorSegundoApellido.isSelected()) {
			columna = "apellido2";
		}
		
		if (rdbtnOrdenarPorNombre.isSelected()) {
			columna = "nombre";
		}
		
		if (rdbtnOrdenarPorPrimerApellido.isSelected()) {
			columna = "apellido1";
		}
		
		if (rdbtnOrdenarporFecha.isSelected()) {
			columna = "fechaNacimiento";
		}
		
		else {
			columna = "id";
		}
		
//// Obtengo todas las personas
		List<Socio> personas = DriverSocio.getInstance().getAllDeEquipo((Equipo) jcEquipo.getSelectedItem(), columna);
//// Preparo una estructura para pasar al constructor de la JTable
//
		/**
		 * Nota: Por cada objeto se creara una filca con la cantidad de columnas
		 * especificadas [0]
		 */

		datos = new Object[personas.size()][5];
//// Cargo los datos de la lista de personas en la matriz de los datos

		/**
		 * Nota: Dever colocar tantos datos como columnas especificadas
		 */

		for (int i = 0; i < personas.size(); i++) {
			Socio persona = personas.get(i);
			datos[i][0] = persona.getNombre();
			datos[i][1] = persona.getApellido1();
			datos[i][2] = persona.getApellido2();
			datos[i][3] = UtilsFecha.DateAString(persona.getFechaNacimiento(), "dd/MM/yyyy");
			datos[i][4] = persona.getId();
			//this.arrayids[i] = persona.getId();
		}
//
		return datos;
	}
	
	public JTable pintarTabla(TableModel tm, JTable table) {
		/**
		 * NOTA la tabra debe de estar en un JScrollPane
		 */
		//////////////////////////////////// TABLA//////////////////////////////////////////////
		tm = new MiTablaModel(getTitulosColumnas(), getDatosDeTabla()); // se accede a los drivers

		table = new JTable(tm);
		
		/*
		 * Nota:
		 * Un estilo diferente de colores por cada clase
		 * */
		table.setDefaultRenderer(String.class, new MiDefaultTableCellRenderer("#0862ff", "#abcaff", "#ebf2ff", "#000000", "#ffffff"));
		table.setDefaultRenderer(Integer.class, new MiDefaultTableCellRenderer("#0862ff", "#abcaff", "#ebf2ff", "#000000", "#ffffff"));
		
		
		// Estilo de los titutlos que se van a poner en tabla
		MiTableHeaderCellRenderer tableHeaderCellRenderer = new MiTableHeaderCellRenderer("#00de5c", "#000000");
		for (int i = 0; i < table.getColumnCount(); i++) {
			table.getColumnModel().getColumn(i).setHeaderRenderer(tableHeaderCellRenderer);
		}
		
		
		return table;
		//////////////////////////////////// TABLA//////////////////////////////////////////////
	}
////////////////////////////////////TABLA//////////////////////////////////////////////

}
