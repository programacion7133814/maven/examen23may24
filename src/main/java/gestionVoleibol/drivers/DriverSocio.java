package gestionVoleibol.drivers;

import java.util.List;

import javax.persistence.Query;

import gestionVoleibol.entidades.Equipo;
import gestionVoleibol.entidades.Socio;

public class DriverSocio extends DriverZ {

	/**********Singleton************/
	private static DriverSocio instance = null;
	
	/**
	 * ****************CONSTRUCTOR****************
	 * En este constructor paso el nombre de la tabla y la clase
	 * que el driver Principal va a manejar
	 */
	public DriverSocio() {
		super(Socio.class, "socio");
		
	}
	
	/****************SINGLETON***************/
	public static DriverSocio getInstance() {
		if (instance == null) {
			instance = new DriverSocio();
		}
		return instance;
	}
	/****************SINGLETON****************/
	
	public List<Socio> getAllDeEquipo(Equipo e, String columna) {
		
		
		
		Query q = this.getentitymanager().createNativeQuery("Select * from socio where idEquipo like " + e.getId() + " order by " + columna, Socio.class);
		return (List<Socio>) q.getResultList();
	}
	
	
}
