package gestionVoleibol.drivers;

import gestionVoleibol.entidades.Equipo;

public class DriverEquipo extends DriverZ {

	/**********Singleton************/
	private static DriverEquipo instance = null;
	
	/**
	 * ****************CONSTRUCTOR****************
	 * En este constructor paso el nombre de la tabla y la clase
	 * que el driver Principal va a manejar
	 */
	public DriverEquipo() {
		super(Equipo.class, "equipo");
		
	}
	
	/****************SINGLETON***************/
	public static DriverEquipo getInstance() {
		if (instance == null) {
			instance = new DriverEquipo();
		}
		return instance;
	}
	/****************SINGLETON***************/
}
