package gestionVoleibol.drivers;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import gestionVoleibol.entidades.Entidad;
import mismetodos.UtilsMenu;

public class DriverZ {
	
	private static EntityManagerFactory etm;
	protected static EntityManager em;
	
	private String nombreTabla;
	private String unidadPersistente = "Examen21may24"; //OJO
	
	//Este es el nombre de la entidad que se va a controlar
	//private String nombreEntidadControl;
	
	//la entidad que se va a controlar
	private Class entidadControlada;

	/**
	 * ***************************CONSTRUCTOR*****************************
	 * @param unidadPersistente Es el nombre de la unidad persistente lo encontras en
	 * 							persistence.xml <persistence-unit name="NOMBRE"transaction-type="RESOURCE_LOCAL">
	 * 
	 * @param entidadControlada Es la "clase" (entidad) que JPA va a manejar
	 * 
	 * @param nombreTabla Esta variabla vendra especificar por el driver que herede de este driver
	 */
	public DriverZ(Class entidadControlada, String nombreTabla) {
		super();
		//Si no existe ninguna etm se crea una nueva
		try {
			if (etm == null) {
				etm = Persistence.createEntityManagerFactory(unidadPersistente);
			}
		} catch (Exception e) {
			e.printStackTrace();
			UtilsMenu.mensajemenu("No se pudo crear un entityManagerFactory");
		}
		
	
		this.entidadControlada = entidadControlada;
		//this.nombreEntidadControl = this.entidadControlada.getName();
		this.nombreTabla = nombreTabla;
	}
	
	
	
	
	/**
	 * Este metodo cierra las conexiones
	 * Normalmente sera llamado al cerrar la aplicacion
	 */
	public static void closeApp() {
		em.close();
		etm.close();
	}
	
	/**
	 * Este metodo recubera una entidad especifica de una tabla
	 * @param id
	 * @return
	 */
	public Entidad findEntity(int id) {
		Entidad e = (Entidad) getentitymanager().find(entidadControlada, id);
		
		return e;
	}
	
	/**
	 * Este metodo inserta um registro en la base de datos
	 * @param e
	 * @param msgError Se duelve el mensaje de error si esta a true
	 * @return 
	 */
	public int persist(Entidad e, boolean msgError) {
		try {
			getentitymanager().getTransaction().begin();
			getentitymanager().persist(e);
			getentitymanager().getTransaction().commit();
			UtilsMenu.mensajemenu("Se guardo correctamenete");
			return e.getId();
		} catch (Exception e2) {
			if (msgError) e2.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * Este metodo borra un registro de la baser de datos
	 * @param e
	 */
	public void delete(Entidad e, boolean msgError) {
		try {
			getentitymanager().getTransaction().begin();
			e = getentitymanager().merge(e); //Con esta linea pongo el foco en la entidad
			getentitymanager().remove(e);
			getentitymanager().getTransaction().commit();
			
		} catch (Exception e2) {
			if (msgError) e2.printStackTrace();
		}
		
	}
	
	/**
	 * Este metodo realiza una actualizacion de un registro
	 * y si no existe lo crea
	 * ****************************
	 * NOTA
	 * Utilizaremos este metodo cuando exista una id;
	 * ****************************
	 */
	public void merge(Entidad e, boolean msgError) {
		
		try {
			getentitymanager().getTransaction().begin();
			getentitymanager().merge(e);
			getentitymanager().getTransaction().commit();
			UtilsMenu.mensajemenu("Se actualizo el registro");
		} catch (Exception e2) {
			if (msgError) e2.printStackTrace();
		}
	}
	
	/**
	 * Este metod compruba su ide mara actualizar o insertar
	 * @param e
	 */
	public void save(Entidad e, boolean msgError) {
		if (e.getId() != 0) {
			merge(e, msgError);
		}
		else {
			persist(e, msgError);
		}
	}
	
	/**
	 * Muestra el pirmer registro de la tabla 
	 * @return
	 */
	public Entidad findFirst() {
		try {
			Query q = getentitymanager().createNativeQuery("select * from " + nombreTabla + " order by id limit 1", entidadControlada);
			return (Entidad) q.getSingleResult();
		} catch (NoResultException e) {
			System.err.println("No se an devuelto resultados");
			e.printStackTrace();
		}
		System.out.println("E devuelto un null");
		return null;
	}
	
	/**
	 * Muestra el ultimo registro de la tabla 
	 * @return
	 */
	public Entidad findlast() {
		try {
			Query q = getentitymanager().createNativeQuery("select * from " + nombreTabla + " order by id desc limit 1", entidadControlada);
			return (Entidad) q.getSingleResult();
		} catch (NoResultException e) {
			System.err.println("No se an devuelto resultados");
			e.printStackTrace();
		}
		System.out.println("E devuelto un null");
		return null;
	}
	
	/**
	 * Muestra el siguiente registro
	 * @return
	 */
	public Entidad findNext(int idActual) {
		try {
			Query q = getentitymanager().createNativeQuery("select * from " + nombreTabla + "  where id > " + idActual + " order by id asc limit 1", entidadControlada);
			return (Entidad) q.getSingleResult();
		} catch (NoResultException nrEx) {
			//nrEx.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Muestra el registro anterior
	 * @return
	 */
	public Entidad findPrevius(int idActual) {
		try {
			Query q = getentitymanager().createNativeQuery("select * from " + nombreTabla + "  where id < " + idActual + " order by id desc limit 1", entidadControlada);
			return (Entidad) q.getSingleResult();
		} catch (NoResultException nrEx) {
			//nrEx.printStackTrace();
		}
		return null;
	}
	
	/**
	 * REcupera todos los registro de una tabla
	 * @return
	 */
	public List<? extends Entidad> getAll() {
		
		Query q = this.getentitymanager().createNativeQuery("Select * from " + nombreTabla, entidadControlada);
		return (List<Entidad>) q.getResultList();
		
	}

	//////////////////////////////////////////
	private static EntityManagerFactory getEtmFactory() {
		return etm;
	}
	
	protected static EntityManager getentitymanager() {
		if (em == null) {
			em = getEtmFactory().createEntityManager();
		}
		return em;
	}
}
