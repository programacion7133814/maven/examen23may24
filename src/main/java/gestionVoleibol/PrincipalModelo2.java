package gestionVoleibol;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import mismetodos.UtilsMenu;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import gestionVoleibol.vista.PanelClasificacion;
import gestionVoleibol.vista.PanelDatosSocio;
import gestionVoleibol.vista.PanelSociosDelEquipo;

/**
 * EN este jframe si coloca un tabbet pane donde van los lo paneles creados
 */
public class PrincipalModelo2 extends JFrame {

	private static final long serialVersionUID = 1L;
	
	/////// TAMAÑO/////////
	private int cordX = 100;
	private int cordY = 100;
	private int tamx = 600;
	private int tamY = 400;
	/////// TAMAÑO/////////

	/****************SINGLETON***************/
	private static PrincipalModelo2 instance = null;
	
	/////////////COMPONENTES///////////////
	private JTabbedPane tabbedPane;
	private JToolBar toolBar;
	////////////////COMPONENTES//////////////
	
	//////////////////PANELES///////////////////
	private PanelDatosSocio pd;
	private PanelSociosDelEquipo psde;
	private PanelClasificacion pc;
	//////////////////PANELES///////////////////
	
	/**
	 * Create the frame.
	 */
	public PrincipalModelo2() {
		super("GestionVoleibol");
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				//Este metodo devuelve un boleano cunado cierra la ventana
				//Si se cierra es true
				UtilsMenu.cierraVentana("Estas seguro que quieres salir", "MenuBBDD", instance);

			}
		});
		setBounds(cordX, cordY, tamx, tamY);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		///////////////////TABBEDPANE/////////////////////
		pd = new PanelDatosSocio();
		psde = new PanelSociosDelEquipo();
		pc = new PanelClasificacion();
		
		tabbedPane.addTab("Datos Socio", pd);
		tabbedPane.addTab("Socios del equipo", psde);
		tabbedPane.addTab("Clasificacion", pc);
		///////////////////TABBEDPANE/////////////////////
		
		toolBar = new JToolBar();
		getContentPane().add(toolBar, BorderLayout.NORTH);
		
		setVisible(true);
	}
	
	////////////////////////////////////
	
	/****************SINGLETON***************/
	public static PrincipalModelo2 getinstance() {
		if (instance == null) instance = new PrincipalModelo2();
		
		return instance;
	}
	/****************SINGLETON***************/
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		getinstance();
	}

}
