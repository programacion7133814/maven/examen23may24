package tabla;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Celdas normales
 */
public class MiDefaultTableCellRenderer extends DefaultTableCellRenderer {
	
	private String colorSelect;
	private String color1;
	private String color2;
	private String colorFuenteSelect;
	private String colorFuente;
	
	/**
	 * Pasa los diferentes colores para que se pinten las celdas
	 * @param colorSelect
	 * @param color1
	 * @param color2
	 * @param colores
	 * @param colorfuente
	 * @param colorFuenteSelect
	 */
	public MiDefaultTableCellRenderer(String colorSelect, String color1, String color2, String colorfuente, String colorFuenteSelect) {
		super();
		this.colorSelect = colorSelect;
		this.color1 = color1;
		this.color2 = color2;
		this.colorFuente = colorfuente;
		this.colorFuenteSelect = colorFuenteSelect;
	}

	/**
	 * Es un metodo de los que se """llaman solos"""
	 * Component es la propia celda
	 * Establece el estilo de la celda
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        c.setForeground(getForegroundColor(table, value, isSelected, hasFocus, row, column));
        c.setBackground(getBackgroundColor(table, value, isSelected, hasFocus, row, column)); //Color de fondo
        return c;
	}
	
	/**
	 * Color basico parar las filas se va alternano entre una y otra
	 * @param table
	 * @param value
	 * @param isSelected
	 * @param hasFocus
	 * @param row
	 * @param column
	 * @return
	 */
	protected Color getBackgroundColor (JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		Color color = null;
		//Graias a esta linea si se selecciona la linaa se pintara de azul
		if (isSelected) {
			return Color.decode(colorSelect);
		}
		else {
			color = row % 2 == 0 ? Color.decode(color1) : Color.decode(color2);
		}
		return color;
	}
	
	/**
	 * Este metodo sirve para dar color a las letras de las celdas
	 * @param table
	 * @param value
	 * @param isSelected
	 * @param hasFocus
	 * @param row
	 * @param column
	 * @return
	 */
	protected Color getForegroundColor(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		Color color = null;
		//Graias a esta linea si se selecciona la linea las letras se pintaran de blanco
		if (isSelected) {
			return Color.decode(colorFuenteSelect);
		}
		else {
			return Color.decode(colorFuente);
		}
	}
	
}
