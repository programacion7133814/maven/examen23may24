package tabla;

import javax.swing.JTable;
import javax.swing.table.TableModel;


public class DatosTabla {
////////////////////////////////////TABLA//////////////////////////////////////////////
	/**
	 * Devuelde los titulos de la columnas
	 * Colocala la misma cantidad de columnas como datos quieras imprimir
	 * 
	 * Ejemplo : return new String[] { "id", "nombre", "apellido1", "apellido2", "dni", "direccion", "email", "telefono" };
	 * @return
	 */
	public static String[] getTitulosColumnas() {
		return new String[] { "id", "nombre", "apellido1", "apellido2", "dni", "direccion", "email", "telefono" };
	}

	/**
	* Este petodo devolvera los metodos de la tabla 
	*/
	public static Object[][] getDatosDeTabla() {
		Object[][] datos = null;
//		// Obtengo todas las personas
//		List<Estudiante> personas = (List<Estudiante>) DriverEstudiante.getInstance().getAll();
//		// Preparo una estructura para pasar al constructor de la JTable
//		
		/**
		 * Nota:
		 * Por cada objeto se creara una filca con la cantidad de columnas especificadas [0]
		 */
		
//		datos = new Object[personas.size()][8];
//		// Cargo los datos de la lista de personas en la matriz de los datos

		/**
		 * Nota:
		 * Dever colocar tantos datos como columnas especificadas
		 */
		
//		for (int i = 0; i < personas.size(); i++) {
//			Estudiante persona = personas.get(i);
//			datos[i][0] = persona.getId();
//			datos[i][1] = persona.getNombre();
//			datos[i][2] = persona.getApellido1();
//			datos[i][3] = persona.getApellido2();
//			datos[i][4] = persona.getDni();
//			datos[i][5] = persona.getDireccion();
//			datos[i][6] = persona.getEmail();
//			datos[i][7] = persona.getTelefono();
//
//		}
//
		return datos;
	}
//////////////////////////////////// TABLA//////////////////////////////////////////////
	
	public static JTable pintarTabla(TableModel tm, JTable table) {
		/**
		 * NOTA la tabra debe de estar en un JScrollPane
		 */
		//////////////////////////////////// TABLA//////////////////////////////////////////////
		tm = new MiTablaModel(getTitulosColumnas(), getDatosDeTabla()); // se accede a los drivers

		table = new JTable(tm);
		
		/*
		 * Nota:
		 * Un estilo diferente de colores por cada clase
		 * */
		/*
		table.setDefaultRenderer(String.class, new MiDefaultTableCellRenderer("#0862ff", "#abcaff", "#ebf2ff", "#000000", "#ffffff"));
		table.setDefaultRenderer(Integer.class, new MiDefaultTableCellRenderer("#0862ff", "#abcaff", "#ebf2ff", "#000000", "#ffffff"));
		table.setDefaultRenderer(Date.class, new MiDefaultTableCellRenderer("#0862ff", "#abcaff", "#ebf2ff", "#000000", "#ffffff"));
		*/
		
		/*
		// Estilo de los titutlos que se van a poner en tabla
		MiTableHeaderCellRenderer tableHeaderCellRenderer = new MiTableHeaderCellRenderer("#00de5c", "#000000");
		for (int i = 0; i < table.getColumnCount(); i++) {
			table.getColumnModel().getColumn(i).setHeaderRenderer(tableHeaderCellRenderer);
		}
		*/
		
		return table;
		//////////////////////////////////// TABLA//////////////////////////////////////////////
	}
	
}
