package tabla;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class MiTableHeaderCellRenderer extends DefaultTableCellRenderer {

	private String colorfondo;
	private String colorfuente;
	
	/**
	 * ******************Constructor*****************
	 * Pasa los colores para que se pinten las celdas del encabezado
	 * @param colorfondo
	 * @param colorFuente
	 */
	public MiTableHeaderCellRenderer(String colorfondo, String colorFuente) {
		super();
		this.colorfondo = colorfondo;
		this.colorfuente = colorFuente;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        JLabel jlabel = (JLabel) c;
        jlabel.setHorizontalAlignment(JLabel.CENTER);
        c.setBackground(Color.decode(colorfondo)); //Color de fondo
        c.setForeground(Color.decode(colorfuente)); //Color de texto
        return c;
	}



}
