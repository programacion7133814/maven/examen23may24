package tabla;

import javax.swing.table.AbstractTableModel;

public class MiTablaModel extends AbstractTableModel {

	//Son los datos de la tabla es una matriz porque tiene dos dimensiones
	Object datos[][] = null;
	//Son los titulos de la tabla
	String titulo[] = null; 
	
	
	/*
	 * **************Constructor*************
	 * Devemos pasarle los titulos y los datos
	 */
	public MiTablaModel(String[] titulo, Object[][] datos) {
		super();
		this.datos = datos;
		this.titulo = titulo;
	}

	public Object[][] getDatos() {
		return datos;
	}

	public void setActualizarDatos(Object[][] datos) {
		this.datos = datos;
		fireTableDataChanged();
	}

	// Los tres siguientes métodos son los mínimos necesarios para representar la tabla
	/**
	 * Permite que la tabla sepa cuántas filas debe mostrar
	 */
	@Override
	public int getRowCount() {
		return datos.length;
	}

	/**
	 * Permite que la tabla sepa cuántas columnas debe mostrar
	 */
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return titulo.length;
	}

	/**
	 * Se conocerá el dato en cada celda, es uno de los métodos fundamentales del abstractTableModel
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return datos[rowIndex][columnIndex];
	}
	
	/**
	 * Este método da nombre a las columnas de la tabla
	 */
	@Override
	public String getColumnName(int column) {
		return this.titulo[column];
	}
	
	
	
	/**
	 * Permite que la tabla sepa que tipo de dato está mostrando en cada columna
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (this.datos.length > 0) {
			return this.datos[0][columnIndex].getClass();
		}
		return String.class;
	}

}
